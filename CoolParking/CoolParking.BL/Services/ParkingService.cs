﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        List<TransactionInfo> currentTransactions = new List<TransactionInfo>();
        Parking parking = Parking.getParking();
        readonly ITimerService withdrawTimer;
        readonly ITimerService logTimer;
        ILogService logService;
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            parking.Vehicles = new List<Vehicle>(Settings.parkingCapacity);
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;
            this.withdrawTimer.Elapsed += WithdrawMethod;
            this.logTimer.Elapsed += LogTimerMethod;
            this.withdrawTimer.Interval = Settings.withdrawTime;
            this.logTimer.Interval = Settings.logTime;
            this.withdrawTimer.Start();
            this.logTimer.Start();
        }
        // метод обработчик события таймера withdrawTimer (Списания денег со счета автомобиля с помощью транзакций)
        // каждый перевод денег должен быть записан в текущий список  транзакций
        // списаная сумма со счета автомобиля должна передоваться на баланс паркинга
        public  void WithdrawMethod(object sender, ElapsedEventArgs e)// возможно не надо делать метод публичным
        {
            lock (parking.Vehicles)
            {
                foreach (Vehicle vehicle in parking.Vehicles)
                {
                    decimal sum;
                    if (vehicle.Balance <= 0)
                        sum = Settings.tariffs[vehicle.VehicleType] * Settings.coefOfTheFine;
                    else if ((vehicle.Balance - Settings.tariffs[vehicle.VehicleType]) < 0)
                        sum = vehicle.Balance + (Settings.tariffs[vehicle.VehicleType] - vehicle.Balance) * Settings.coefOfTheFine;
                    else
                        sum = Settings.tariffs[vehicle.VehicleType];

                    vehicle.Balance -= sum;
                    lock (currentTransactions)
                    {
                        currentTransactions.Add(new TransactionInfo(vehicle.Id, sum, DateTime.Now));
                    }
                    parking.Balance += sum;
                }
            }
        }
        // метод обработчик события таймера logTimer (запись в лог)
        // метод должен записывать в лог список всех текущих транзакций и освобождать этот список
        public  void LogTimerMethod(object sender, ElapsedEventArgs e)// возможно не надо делать метод публичным
        {
            string loginfo = "";
            lock (currentTransactions)
            {
                foreach (TransactionInfo transaction in currentTransactions)
                {
                    loginfo += $"{transaction.Id} {transaction.Sum} {transaction.Time}\n";
                }
                lock (Settings.fileLocker)
                {
                    logService.Write(loginfo);
                }
                currentTransactions.Clear();
            }
        }
        public void AddVehicle(Vehicle vehicle)
        {
            foreach (Vehicle _vehicle in parking.Vehicles)
            {
                if (_vehicle.Id == vehicle.Id)
                    throw new ArgumentException("Invalid vehicle id. A vehicle with the same id already exists.");
            }
            if (parking.Vehicles.Count < Settings.parkingCapacity)
                lock (parking.Vehicles)
                {
                    parking.Vehicles.Add(vehicle);
                }
            else throw new InvalidOperationException("Invalid add vehicle id. Parking is full");
        }
        public void Dispose()
        {
            parking.Balance=0;
            parking.Vehicles = null;
            withdrawTimer.Dispose();
            logTimer.Dispose();
            logService = null;
            GC.SuppressFinalize(this);
        }
        public decimal GetBalance()
        {
            return parking.Balance;
        }
        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }
        public int GetFreePlaces()
        {
            return Settings.parkingCapacity - parking.Vehicles.Count;
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return currentTransactions.ToArray();
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }
        public string ReadFromLog()
        {
            lock (Settings.fileLocker)
            {
                return logService.Read();
            }
        }
        public void RemoveVehicle(string vehicleId)
        {
            foreach (Vehicle _vehicle in parking.Vehicles)
            {
                if (_vehicle.Id == vehicleId)
                {
                    // нет в тестах
                    if (_vehicle.Balance < 0)
                        throw new InvalidOperationException("Invalid remove vehicle. This vehicle has negative balance. You need to pay off debt.");
                    parking.Vehicles.Remove(_vehicle);
                    return;
                }
            }
            throw new ArgumentException("Invalid vehicle id. There is no vehicle with such ID in the parking");
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Invalid sum. It is forbidden to replenish the balance for a negative sum.");

            foreach (Vehicle _vehicle in parking.Vehicles)
            {
                if (_vehicle.Id == vehicleId)
                {
                    _vehicle.Balance += sum;
                    return;
                }
            }
            throw new ArgumentException("Invalid vehicle id. There is no vehicle with such ID in the parking");
        }
    }
}