﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
//-1 Вивести на екран поточний баланс Паркінгу.
//-2 Вивести на екран суму зароблених коштів за поточний період (до запису у лог).
//-3 Вивести на екран кількість вільних місць на паркуванні(вільно X з Y).
//-4 Вивести на екран усі Транзакції Паркінгу за поточний період(до запису у лог).
//-5 Вивести на екран історію Транзакцій(зчитавши дані з файлу Transactions.log).
//-6 Вивести на екран список Тр.засобів , що знаходяться на Паркінгу.
//-7 Поставити Транспортний засіб на Паркінг.
//-8 Забрати Транспортний засіб з Паркінгу.
//-9 Поповнити баланс конкретного Тр. засобу.
namespace UserInterface
{
    class Program
    {
        static ParkingService parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService("Transaction.log"));
       
        #region Menu

        static System.Collections.Generic.List<string> menuMain = new System.Collections.Generic.List<string>() 
        {
            "Parking info...",
            "Add vehicle",
            "Remove vehicle",
            "Top up vehicle\n\n",
            "Exit"
        };
        static System.Collections.Generic.List<string> menuParking = new System.Collections.Generic.List<string>()
        {
            "Current balance","Number of free parking places",
            "Sum of profit for the current period",
            "List of vehicles in the parking",
            "Transactions info...\n\n",
            "Return"
        };
        static System.Collections.Generic.List<string> menuTransactions = new System.Collections.Generic.List<string>()
        {
            "Last parking transactions",
            "Transactions history\n\n",
            "Return"
        };
        private static int index = -1;
        static void DrawCoolParking()
        {
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(new string(' ', 93));
            Console.WriteLine(new string(' ', 3) + "░█████╗░░█████╗░░█████╗░██╗░░     ██████╗░░█████╗░██████╗░██╗░░██╗██╗███╗░░██╗░██████╗░" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "██╔══██╗██╔══██╗██╔══██╗██║░░     ██╔══██╗██╔══██╗██╔══██╗██║░██╔╝██║████╗░██║██╔════╝░" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "██║░░╚═╝██║░░██║██║░░██║██║░░     ██████╔╝███████║██████╔╝█████═╝░██║██╔██╗██║██║░░██╗░" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "██║░░██╗██║░░██║██║░░██║██║░░     ██╔═══╝░██╔══██║██╔══██╗██╔═██╗░██║██║╚████║██║░░╚██╗" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "╚█████╔╝╚█████╔╝╚█████╔╝███████╗  ██║░░░░░██║░░██║██║░░██║██║░╚██╗██║██║░╚███║╚██████╔╝" + new string(' ', 3) + "\n" +
                              new string(' ', 3) + "░╚════╝░░╚════╝░░╚════╝░╚══════╝  ╚═╝░░░░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝╚═╝╚═╝░░╚══╝░╚═════╝░" + new string(' ', 3));
            Console.WriteLine(new string(' ', 93));
            Console.ResetColor();
        }
        private static int drawMenu(System.Collections.Generic.List<string> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (i == index)
                {
                    //Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(new string(' ', 6) + "->  " + items[i]);
                }
                else
                {
                    Console.WriteLine(new string(' ', 10) + items[i]);
                }
                Console.ResetColor();
            }
            ConsoleKeyInfo key = Console.ReadKey();
            switch (key.Key)
            {
                case ConsoleKey.DownArrow:
                    if (index == items.Count - 1) index = 0;
                    else index++;
                    break;
                case ConsoleKey.UpArrow:
                    if (index <= 0) index = items.Count - 1;
                    else index--;
                    break;
                case ConsoleKey.Enter:
                    return index + 1;
                default:
                    Console.Clear();
                    return 0;
            }
            Console.Clear();
            return 0;
        }
        private static void DrawMainMenu()
        {
            while (true)
            {
                DrawCoolParking();
                Console.SetCursorPosition(0, 13);
                int selectedMenuItem = drawMenu(menuMain);
                switch (selectedMenuItem)
                {
                    case 1:
                        DrawParkingMenu();
                        break;
                    case 2:
                        Console.Clear();
                        AddVehicle();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        RemoveVehicle();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();
                        TopUpBalance();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }

            }
        }
        static void DrawParkingMenu()
        {
            index = -1;
            Console.Clear();
            while (true)
            {
                DrawCoolParking();
                Console.SetCursorPosition(0, 13);
                int selectedMenuParking = drawMenu(menuParking);
                switch (selectedMenuParking)
                {
                    case 1:
                        Console.Clear();
                        YellowPrint($"Curent balance of the parking: {parkingService.GetBalance()}");
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        YellowPrint($"Number of free parking places:  free {parkingService.GetFreePlaces()} with {parkingService.GetCapacity()}");
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        YellowPrint($"Current income: {CurrentProfit()}");
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();
                        YellowPrint("List of vehicles:\n");
                        PrintListOfVehicles();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        DrawTransactionMeny();
                        break;
                    case 6:
                        Console.Clear();
                        index = -1;
                        DrawMainMenu();
                        break;
                    default:
                        break;
                }

            }
        }
        static void DrawTransactionMeny()
        {
            index = -1;
            Console.Clear();
            while (true)
            {
                DrawCoolParking();
                Console.SetCursorPosition(0, 13);
                int selectedmenuTransactions = drawMenu(menuTransactions);
                switch (selectedmenuTransactions)
                {
                    case 1:
                        Console.Clear();
                        YellowPrint("Parking Transactions for the current period: ");
                        PrintLastParkingTransactions();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        YellowPrint("Transaction history:");
                        ReadLog();
                        Console.WriteLine("Pres any key...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        index = -1;
                        DrawParkingMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region servises
        static void ReadLog()
        {
            try
            {
                Console.WriteLine(parkingService.ReadFromLog());
            }
            catch (InvalidOperationException e)
            {
                RedPrint(e.Message);
            }
        }
        static void PrintListOfVehicles()
        {
            foreach(Vehicle vehicle in parkingService.GetVehicles())
                Console.WriteLine(vehicle);
            Console.WriteLine();
        }
        static void PrintVehicleTypes()
        {
            string vehicleTypes = $"1. {VehicleType.PassengerCar}\n"+
                $"2. {VehicleType.Truck}\n" +
                $"3. {VehicleType.Bus}\n" +
                $"4. {VehicleType.Motorcycle}\n";
            Console.WriteLine(vehicleTypes);
        }
        static void AddVehicle()
        {
            while (true)
            {
                try
                {
                    string id;
                    VehicleType type;
                    decimal balance;
                    int numberOfVehicleType;
                    Console.Write("Enter vehicle id: ");
                    id = Console.ReadLine();
                    Console.WriteLine("\n  Vehicle Types:");
                    PrintVehicleTypes();
                    while (true)
                    {
                        Console.Write("Enter number of vehicle type: ");
                        try
                        {
                            numberOfVehicleType = Convert.ToInt32(Console.ReadLine());
                            if (numberOfVehicleType > 0 && numberOfVehicleType < 5)
                                break;
                            else RedPrint("Invalid number od vehicle type");
                        }
                        catch (FormatException)
                        {
                            RedPrint("Inaccessible format.");
                        }
                    }
                    type = (VehicleType)numberOfVehicleType;
                    while (true)
                    {
                        try
                        {
                            Console.Write("Enter vehicle balance: ");
                            balance = Convert.ToDecimal(Console.ReadLine());
                            break;
                        }
                        catch (FormatException)
                        {
                            RedPrint("Inaccessible format.");
                        }
                    }
                    parkingService.AddVehicle(new Vehicle(id, type, balance));
                    GreenPrint("Successfully");
                    break;
                }
                catch (ArgumentException e)
                {
                    RedPrint(e.Message);
                }
                catch(InvalidOperationException e)
                {
                    RedPrint(e.Message);
                    break;
                }
            }
            Console.WriteLine();
        }
        static void RemoveVehicle()
        {
            //while (true)
            {
                try
                {
                    string id;
                    Console.Write("Enter vehicle id: ");
                    id = Console.ReadLine();
                    parkingService.RemoveVehicle(id);
                    GreenPrint("Successfully");
                    //break;
                }
                catch (ArgumentException e)
                {
                    RedPrint(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    RedPrint(e.Message);
                    //break;
                }
            }
        }
        static void TopUpBalance()
        {
            //while (true)
            {
                try
                {
                    string id;
                    decimal sum;
                    Console.Write("Enter vehicle id: ");
                    id = Console.ReadLine();
                    while (true)
                    {
                        try
                        {
                            Console.Write("Enter sum: ");
                            sum = Convert.ToDecimal(Console.ReadLine());
                            break;
                        }
                        catch (FormatException)
                        {
                            RedPrint("Inaccessible format.");
                        }
                    }
                    parkingService.TopUpVehicle(id,sum);
                    GreenPrint("Successfully");
                    //break;
                }
                catch (ArgumentException e)
                {
                    RedPrint(e.Message);
                }
            }
            Console.WriteLine();
        }
        static void PrintLastParkingTransactions()
        {
            foreach (var transactionInfo in parkingService.GetLastParkingTransactions())
            {
                Console.WriteLine(transactionInfo);
            }
        }
        static decimal CurrentProfit()
        {
            decimal income = 0;
            foreach (TransactionInfo transactionInfo in parkingService.GetLastParkingTransactions())
            {
                income += transactionInfo.Sum;
            }
            return income;
        }

        #endregion

        #region color print

        static void YellowPrint(string str)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(str);
            Console.ResetColor();
        }
        static void RedPrint(string str)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(str);
            Console.ResetColor();
        }
        static void GreenPrint(string str)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(str);
            Console.ResetColor();
        }

        #endregion
        private static void Main(string[] args)
        {
            Console.SetWindowSize(93, 30);
            Console.CursorVisible = false;
            while (true)
            {
                DrawMainMenu();
            }
        }
    }
}